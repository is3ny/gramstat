package main

import (
	"flag"
	"fmt"
	"os"

	gramStat "kureduro.me/ngraphstat"
)

var granularity     int
var inputFilepath   string
var outputFilepath  string
var verbose         bool

func init() {
    flag.IntVar(&granularity, "n", 2, "the length of the substrings to be analyzed")
    flag.StringVar(&inputFilepath, "i", "", "the filename of a dictionary file with one word per line (default stdin)")
    flag.StringVar(&outputFilepath, "o", "", "output file (default stdout)")
    flag.BoolVar(&verbose, "v", false, "print how many times each substring was encountered")
}

func openStream(filepath string, dflt *os.File) (*os.File, func(), error) {
    closeFunc := func() {}
    stream := dflt

    if filepath != "" {
        var err error
        stream, err = os.Open(filepath)
        if err != nil {
            return nil, closeFunc, fmt.Errorf("error opening %s: %v\n", filepath, err)
        }

        closeFunc = func() {
            stream.Close()
        }       
    }

    return stream, closeFunc, nil
}

func main() {
    
    flag.Parse()

    inputStream, closeInput, err := openStream(inputFilepath, os.Stdin)
    if err != nil {
        fmt.Fprintf(os.Stderr, "%v\n", err)
        return
    }
    defer closeInput()

    outputStream, closeOutput, err := openStream(outputFilepath, os.Stdout)
    if err != nil {
        fmt.Fprintf(os.Stderr, "%v\n", err)
    }
    defer closeOutput()

    words := gramStat.LoadWords(inputStream)
    stat, err := gramStat.Analyze(words, granularity)
    if err != nil {
        fmt.Fprintf(os.Stderr, "irrecoverable error occured when analyzing data: %v", err)
        return
    }

    words = stat.ToSortedWordList()

    if verbose {
        for _, word := range words {
            fmt.Fprintln(outputStream, word, stat[word])
        }

        return
    }

    fmt.Fprintln(outputStream, words)
}
