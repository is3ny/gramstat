package gramStat_test

import (
	"fmt"
	"strings"
	"testing"

	gramStat "kureduro.me/ngraphstat"
)

func TestWordListStringer(t *testing.T) {
    
    cases := []struct {
        description string
        list gramStat.WordList
        want string
    }{
        {
            "two words",
            gramStat.WordList{"spell", "card"},
            "spell card",
        },
        {
            "five words",
            gramStat.WordList{"this", "spell", "card", "is", "hard"},
            "this spell card is hard",
        },
        {
            "two words",
            gramStat.WordList{"spell", "card"},
            "spell card",
        },
        {
            "one null word",
            gramStat.WordList{""},
            "",
        },
        {
            "empty",
            gramStat.WordList{},
            "",
        },
    }

    for _, test := range cases {
        t.Run(test.description,
        func(t *testing.T) {
            str := test.list.String()
            gramStat.AssertString(t, str, test.want)
        })
    }
}

func TestWordLoading(t *testing.T) {
    
    cases := []struct {
        input string
        want gramStat.WordList
    }{
        {
            "a\nb\nc\n",
            gramStat.WordList{"a", "b", "c"},
        },
        {
            "x\ny\nz\nw\n",
            gramStat.WordList{"x", "y", "z", "w"},
        },
        {
            "\n\n",
            gramStat.WordList{},
        },
        {
            "",
            gramStat.WordList{},
        },
    }

    for _, test := range cases {
        t.Run(fmt.Sprintf("parse %q", test.input),
        func(t *testing.T) {
            reader := strings.NewReader(test.input)
            got := gramStat.LoadWords(reader)
            
            gramStat.AssertWordList(t, got, test.want)
        })
    }
}

func TestAnalyze(t *testing.T) {

    cases := []struct {
        in          gramStat.WordList
        granularity int
        want        gramStat.FrequencyMap
        err         error 
    }{
        {
            in:   gramStat.WordList{"abc", "bcd", "cde"},
            granularity: 1,
            want: gramStat.FrequencyMap{
                "a": 1,
                "b": 2,
                "c": 3,
                "d": 2,
                "e": 1,
            },
            err: nil,
        },
        {
            in:   gramStat.WordList{"abc", "bcd", "cde"},
            granularity: 2,
            want: gramStat.FrequencyMap{
                "ab": 1,
                "bc": 2,
                "cd": 2,
                "de": 1,
            },
            err: nil,
        },
        {
            in:   gramStat.WordList{"abc", "bcd", "cde"},
            granularity: 3,
            want: gramStat.FrequencyMap{
                "abc": 1,
                "bcd": 1,
                "cde": 1,
            },
            err: nil,
        },
        {
            in:   gramStat.WordList{"abc", "bcd", "cde"},
            granularity: 4,
            want: gramStat.FrequencyMap{},
            err: nil,
        },
        {
            in:   gramStat.WordList{"a", "ab", "abc"},
            granularity: 3,
            want: gramStat.FrequencyMap{
                "abc": 1,
            },
            err: nil,
        },
        {
            in:   gramStat.WordList{"a", "b", "c"},
            granularity: 0,
            want: gramStat.FrequencyMap{},
            err: gramStat.ErrInvalidGranularity,
        },
        {
            in:   gramStat.WordList{"ab", "bcd"},
            granularity: -2,
            want: gramStat.FrequencyMap{},
            err: gramStat.ErrInvalidGranularity,
        },
        {
            in:   gramStat.WordList{"abstract", "stuff", "is", "very", "very", "fun"},
            granularity: 3,
            want: gramStat.FrequencyMap{
                "fun": 1,
                "abs": 1,
                "bst": 1,
                "str": 1,
                "tra": 1,
                "rac": 1,
                "act": 1,
                "stu": 1,
                "tuf": 1,
                "uff": 1,
                "ver": 2,
                "ery": 2,
            },
            err: nil,
        },
        {
            in:   gramStat.WordList{"shhhhhh", "there's", "a", "b-e-a-r"},
            granularity: 2,
            want: gramStat.FrequencyMap{
                "sh": 1,
                "hh": 5,
                "th": 1,
                "he": 1,
                "er": 1,
                "re": 1,
                "e'": 1,
                "'s": 1,
                "b-": 1,
                "-e": 1,
                "e-": 1,
                "-a": 1,
                "a-": 1,
                "-r": 1,
            },
            err: nil,
        },
        {
            in:   gramStat.WordList{"aaaaaa", "aaaaaaa"},
            granularity: 2,
            want: gramStat.FrequencyMap{
                "aa": 11,
            },
            err: nil,
        },
    }

    for _, test := range cases {
        t.Run(fmt.Sprintf("%v||%d", test.in, test.granularity),
        func(t *testing.T) {
            stat, err := gramStat.Analyze(test.in, test.granularity)

            gramStat.AssertError(t, err, test.err)
            gramStat.AssertFrequencyMap(t, stat, test.want)
        })
    }
}

func TestFrequencyMapToWordList(t *testing.T) {

    t.Run("equal frequency substrings are sorted",
    func(t *testing.T) {
        fm := gramStat.FrequencyMap{
            "b":  1,
            "bc": 1,
            "ba": 1,
            "a":  1,
            "z":  1,
            "y":  1,
            "i":  1,
        }
        got := fm.ToSortedWordList()

        want := gramStat.WordList{"a", "b", "ba", "bc", "i", "y", "z"}

        gramStat.AssertWordList(t, got, want)
    })

    t.Run("more frequent substrings appear sooner",
    func(t *testing.T) {
        fm := gramStat.FrequencyMap{
            "b": 1,
            "a": 4,
            "z": 2,
            "y": 3,
            "i": 5,
        }
        got := fm.ToSortedWordList()

        want := gramStat.WordList{"i", "a", "y", "z", "b"}

        gramStat.AssertWordList(t, got, want)
    })

    t.Run("most frequent appear first, ones with equal frequency are lexicographically sorted",
    func(t *testing.T) {
        fm := gramStat.FrequencyMap{
            "abstr": 5,
            "lost": 4,
            "lust": 4,
            "last": 4,
            "loss": 4,
            "loser": 4,
            "but": 3,
            "station": 1,
            "incarnate": 1,
        }
        got := fm.ToSortedWordList()

        want := gramStat.WordList{"abstr", "last", "loser", "loss", "lost", "lust", "but", "incarnate", "station"}

        gramStat.AssertWordList(t, got, want)
    })
}
