package gramStat

import (
	"bufio"
	"errors"
	"io"
	"sort"
	"strings"
)

var ErrInvalidGranularity = errors.New("analysis of [0 or less]-grams cannot be performed")

type FrequencyMap map[string]int

type WordList []string

type fmEntry struct {
    substr    string
    frequency int
}

type fmEntries []fmEntry

func (e fmEntries) less(i, j int) bool {
    if e[i].frequency == e[j].frequency {
        return e[i].substr < e[j].substr
    }

    return e[i].frequency > e[j].frequency
}

func (w WordList) String() string {
    if len(w) == 0 {
        return ""
    }

    var str strings.Builder

    for i := range w {
        str.WriteString(w[i])
        str.WriteString(" ")
    }

    return str.String()[:str.Len()-1]
}

func LoadWords(r io.Reader) WordList {
    list := WordList{}
    reader := bufio.NewScanner(r)
    for reader.Scan() {
        text := reader.Text()
        if len(text) == 0 {
            continue
        }

        list = append(list, text)
    }
    return list
}

func Analyze(words WordList, n int) (FrequencyMap, error) {
    fm := FrequencyMap{}

    if n < 1 {
        return fm, ErrInvalidGranularity
    }

    for i := range words {
        for j := 0; j < len(words[i]) - n + 1; j++ {
            fm[words[i][j:j + n]]++
        }
    }
    return fm, nil
}

func (f FrequencyMap) ToSortedWordList() WordList {
    entries := make(fmEntries, len(f))

    i := 0
    for k, v := range f {
        entries[i] = fmEntry{k, v}
        i++
    }

    sort.Slice(entries, entries.less)

    words := make(WordList, len(entries))
    for i := range entries {
        words[i] = entries[i].substr
    }

    return words
}
