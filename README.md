# Gram Stat

I think one example worths more than any words...

```
$ cat dictionary.txt
abstract
structure
abstruction
distinction
information
decision
essence
```

```
$ ./gramstat -n 3 -i dictionary.txt
ion str tio abs bst cti ruc tru uct act ati cis ctu dec dis eci enc ess for inc inf isi ist mat nce nct nfo orm rac rma sen sio sse sti tin tra tur ure
$ ./gramstat -n 3 -i dictionary.txt -v
ion 4
str 3
tio 3
abs 2
bst 2
cti 2
ruc 2
tru 2
uct 2
act 1
ati 1
cis 1
ctu 1
dec 1
dis 1
eci 1
enc 1
ess 1
for 1
inc 1
inf 1
isi 1
ist 1
mat 1
nce 1
nct 1
nfo 1
orm 1
rac 1
rma 1
sen 1
sio 1
sse 1
sti 1
tin 1
tra 1
tur 1
ure 1
```