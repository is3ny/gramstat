package gramStat

import (
	"reflect"
	"testing"
)

func AssertString(t *testing.T, got, want string) {
    t.Helper()

    if got != want {
        t.Errorf("got %q, want %q", got, want)
    }
}

func AssertError(t *testing.T, got, want error) {
    t.Helper()
    if got != want {
        if want == nil {
            t.Fatalf("got error %v, but expected no error", got)
        } else {
            t.Fatalf("got error %v, expected %v", got, want)
        }
    }
}

func AssertFrequencyMap(t *testing.T, got, want FrequencyMap) {
    t.Helper()
    if !reflect.DeepEqual(got, want) {
        t.Errorf("got analysis %v, want %v", got, want)
    }
}

func AssertWordList(t *testing.T, got, want WordList) {
    t.Helper()
    if !reflect.DeepEqual(got, want) {
        t.Errorf("got word list %v, want %v", got, want)
    }
}
