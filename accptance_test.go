package gramStat_test

import (
	"strings"
	"testing"
    "kureduro.me/ngraphstat"
)

func TestAnalysis(t *testing.T) {
    words := "abstract\nalgebra\nfield\nring\ngroup\nstall\n"
    wordsReader := strings.NewReader(words)
    dict := gramStat.LoadWords(wordsReader)
    stat, err := gramStat.Analyze(dict, 2)

    gramStat.AssertError(t, err, nil)

    freq := stat.ToSortedWordList()

    got := freq.String()
    want := "al ra st ab ac br bs ct eb el fi ge gr ie in ld lg ll ng ou ri ro ta tr up"

    if got != want {
        t.Errorf("got %q, want %q, input %q", got, want, words)
    }
}
